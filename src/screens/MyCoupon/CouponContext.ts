import {CouponItme} from '@src/types/Coupon';
import {createContext} from 'react';

export interface CouponAction {
  selectItem?: CouponItme;
  setSelectItem(item: CouponItme): void;
}

export const CouponContext = createContext<CouponAction>({} as CouponAction);
