import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import React, {createContext, FC, useEffect, useRef, useState} from 'react';
import {styles} from './styles';
import CouponList, {CouponListRef} from '@src/components/MyCoupon/CouponList';
import SwitchTab from '@src/components/MyCoupon/SwitchTab';
import CouponFoot from '@src/components/MyCoupon/CouponFoot';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BottomButton from '@src/components/MyCoupon/BottomButton';
import {getUserCouponTypes} from '@src/api/MyCoupon';
import {CouponItme, CouponTypeItem} from '@src/types/Coupon';
import {
  getCouponList as GetCouponList,
  GetCouponListParams,
} from '@src/api/MyCoupon';
import {$event} from '@src/utils/eventEmitter';
import {PropProfileScreenRouteProps} from '@src/navigation';
import {CouponContext} from './CouponContext';

const MyCoupon: FC<PropProfileScreenRouteProps<'MyCoupon'>> = ({
  route,
  navigation,
}) => {
  const {type, order} = route.params;
  const [typtActive, setTyptActive] = useState(0);
  const [showAllSort, setShowAllSort] = useState(false);
  const [selectItem, setSelectItem] = useState<CouponItme>();
  const [couponTypeList, setCouponTypeList] = useState<CouponTypeItem[]>([]);
  const [couponList, setCouponList] = useState<CouponItme[]>([]);
  const childRef = useRef<CouponListRef>(null);
  const pageIndex = useRef(1);
  const EnterpriseId = useRef(0);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    // 获取优惠券类型
    getCouponTypeList();
    // 获取优惠券列表
    getCouponList();
    return () => {
      $event.removeByType('orderSelectionCoupon');
      setCouponList([]);
      setCouponTypeList([]);
    };
  }, []);

  // 获取优惠券类型List
  const getCouponTypeList = async () => {
    const {CouponTypeList} = await getUserCouponTypes();
    console.log('CouponTypeList', CouponTypeList);
    setTyptActive(CouponTypeList[0].CouponType);
    setCouponTypeList(CouponTypeList);
  };

  // 获取优惠券列表
  const getCouponList = (p?: Partial<GetCouponListParams>) =>
    GetCouponList(
      Object.assign(
        {
          UserId: 0, //用户id
          EnterpriseId: p?.EnterpriseId || EnterpriseId.current, //企业券传企业id，个人券传0；历史优惠券传0
          PageIndex: pageIndex.current, //页码
          PageSize: 10, //每页数据条数
          PriceToken: '', //计价token，传空字符传 ""
          OutDate: 1 || -2, //历史优惠券（1正常可用，-1过期，-2已使用）
          CouponType: typtActive, //0跑腿券，1商品券
        },
        p,
      ) as GetCouponListParams,
    ).then(res => {
      setCouponList(res.CouponList);
    });

  // 顶部切换
  const switchChange = (index: number) => {
    setCouponList([]);
    setCouponListRefreshing(true);
    pageIndex.current = 1;
    EnterpriseId.current = index ? 20054 : 0;
    getCouponList({EnterpriseId: index ? 20054 : 0}).finally(() =>
      setCouponListRefreshing(false),
    );
  };

  // 设置ListRefreshing状态
  const setCouponListRefreshing = (b: boolean) => {
    childRef.current?.setRefreshing && childRef.current?.setRefreshing(b);
  };

  const handleTypeChange = (i: CouponTypeItem, b?: boolean) => {
    pageIndex.current = 1;
    b && setShowAllSort(false);
    setTyptActive(i.CouponType);
    setCouponListRefreshing(true);
    getCouponList({CouponType: i.CouponType}).finally(() =>
      setCouponListRefreshing(false),
    );
  };

  const handleCouponListRefresh = async () => {
    pageIndex.current = 1;
    await getCouponList();
  };

  // 触底加载
  const handleEndReached = () => {
    if (isLoading) return;
    pageIndex.current++;
    setIsLoading(true);
    GetCouponList({
      UserId: 0, //用户id
      EnterpriseId: 0, //企业券传企业id，个人券传0；历史优惠券传0
      PageIndex: pageIndex.current, //页码
      PageSize: 10, //每页数据条数
      PriceToken: '', //计价token，传空字符传 ""
      OutDate: 1, //历史优惠券（1正常可用，-1过期，-2已使用）
      CouponType: typtActive, //0跑腿券，1商品券
    })
      .then(res => {
        setCouponList(e => [...e, ...res.CouponList]);
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <CouponContext.Provider value={{selectItem, setSelectItem}}>
      <View>
        {type === 'merchant' && (
          <SwitchTab items={['个人券', '企业券']} onChange={switchChange} />
        )}
        {/* 优惠券分类选择 */}
        <View style={styles.typeListBox}>
          <ScrollView
            style={styles.typeList}
            contentContainerStyle={styles.typeList.contentContainer}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            alwaysBounceHorizontal={false}>
            {couponTypeList.map((item, i) => (
              <TouchableOpacity
                key={i}
                activeOpacity={0.5}
                onPress={() => handleTypeChange(item)}>
                <View
                  key={i}
                  style={[
                    styles.typeList.typeItem,
                    typtActive === item.CouponType
                      ? styles.typeList.typeItemAc
                      : {},
                  ]}>
                  <Text
                    style={[
                      styles.typeList.typeText,
                      typtActive === item.CouponType
                        ? styles.typeList.typeItemAcT
                        : {},
                    ]}>
                    {item.Name}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </ScrollView>
          <TouchableWithoutFeedback
            onPress={() => {
              setShowAllSort(e => !e);
            }}>
            <View style={styles.menuunfold}>
              <AntDesign name="menuunfold" size={15} />
            </View>
          </TouchableWithoutFeedback>
        </View>
        {/* 全部分类弹窗 */}
        {showAllSort && (
          // 动态调整top
          <View style={[styles.allSort, type === 'user' ? {top: -5} : {}]}>
            <View style={styles.allSort.content}>
              <View style={styles.allSort.titleBox}>
                <Text style={styles.allSort.title}>全部分类</Text>
                <TouchableWithoutFeedback
                  onPress={() => {
                    setShowAllSort(e => !e);
                  }}>
                  <View style={styles.allSort.menuunfold}>
                    {/* <AntDesign name="menuunfold" size={15} /> */}
                  </View>
                </TouchableWithoutFeedback>
              </View>
              <View style={styles.allSort.list}>
                {/* 分类列表 */}
                {couponTypeList.map((item, i) => (
                  <TouchableOpacity
                    key={i}
                    activeOpacity={0.5}
                    onPress={() => handleTypeChange(item, true)}>
                    <View
                      key={i}
                      style={[
                        styles.typeList.typeItem,
                        typtActive === item.CouponType
                          ? styles.typeList.typeItemAc
                          : {},
                        styles.allSort.listItem,
                      ]}>
                      <Text
                        style={[
                          styles.typeList.typeText,
                          typtActive === item.CouponType
                            ? styles.typeList.typeItemAcT
                            : {},
                        ]}>
                        {item.Name}
                      </Text>
                    </View>
                  </TouchableOpacity>
                ))}
              </View>
            </View>
            <TouchableWithoutFeedback
              onPress={() => {
                console.log('setShowAllSort');
                setShowAllSort(e => !e);
              }}>
              <View style={styles.allSort.bg}>{}</View>
            </TouchableWithoutFeedback>
          </View>
        )}

        {/* 优惠券列表 */}
        <CouponList
          ref={childRef}
          itemType={order ? 4 : 1}
          data={couponList}
          selectItem={selectItem}
          handleCouponItemClick={item => {
            setSelectItem(item);
          }}
          onRefresh={handleCouponListRefresh}
          onEndReached={handleEndReached}
          navigation={navigation}
          ListFooterComponent={<CouponFoot navigation={navigation} />}
        />
      </View>

      {/* 选择优惠券按钮 */}
      {order && (
        <BottomButton
          selectItem={selectItem}
          confirm={item => {
            if (item) {
              $event.emit('orderSelectionCoupon', item);
              navigation.goBack();
            }
          }}
        />
      )}
    </CouponContext.Provider>
  );
};

export default MyCoupon;
