import {View, Text} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import SwitchTab from '@src/components/MyCoupon/SwitchTab';
import CouponList, {CouponListRef} from '@src/components/MyCoupon/CouponList';
import {CouponItme} from '@src/types/Coupon';
import {
  getCouponList as GetCouponList,
  GetCouponListParams,
} from '@src/api/MyCoupon';

type Props = {};

const HistoricalCoupons = (props: Props) => {
  const [ListEmptyText, setListEmptyText] = useState('您当前没有已使用优惠券');
  const [couponList, setCouponList] = useState<CouponItme[]>([]);
  const pageIndex = useRef(1);
  const [isLoading, setIsLoading] = useState(false);

  const [outDateType, setOutDateType] =
    useState<GetCouponListParams['OutDate']>(-2);
  const childRef = useRef<CouponListRef>(null);

  useEffect(() => {
    // 获取优惠券列表
    getCouponList();
    return () => {
      setCouponList([]);
    };
  }, []);

  const getCouponList = (p?: Partial<GetCouponListParams>) =>
    GetCouponList({
      UserId: 0, //用户id
      EnterpriseId: 0, //企业券传企业id，个人券传0；历史优惠券传0
      PageIndex: pageIndex.current, //页码
      PageSize: 10, //每页数据条数
      PriceToken: '', //计价token，传空字符传 ""
      OutDate: p?.OutDate || outDateType, //历史优惠券（1正常可用，-1过期，-2已使用）
      CouponType: 0, //0跑腿券，1商品券
    }).then(res => {
      setCouponList(res.CouponList);
    });

  const handleSwitchTabChange = (i: number) => {
    setCouponList([]);
    setListEmptyText(i ? '您当前没有已失效优惠券' : '您当前没有已使用优惠券');
    pageIndex.current = 1;
    setOutDateType(i === 0 ? -2 : -1);
    setCouponListRefreshing(true);
    getCouponList({OutDate: i === 0 ? -2 : -1}).finally(() =>
      setCouponListRefreshing(false),
    );
  };

  const setCouponListRefreshing = (b: boolean) => {
    childRef.current?.setRefreshing && childRef.current?.setRefreshing(b);
  };

  // 下拉刷新
  const handleCouponListRefresh = async () => {
    pageIndex.current = 1;
    await getCouponList();
  };
  // 触底加载
  const handleEndReached = () => {
    if (isLoading) return;
    pageIndex.current++;
    setIsLoading(true);
    GetCouponList({
      UserId: 0, //用户id
      EnterpriseId: 0, //企业券传企业id，个人券传0；历史优惠券传0
      PageIndex: pageIndex.current, //页码
      PageSize: 10, //每页数据条数
      PriceToken: '', //计价token，传空字符传 ""
      OutDate: outDateType, //历史优惠券（1正常可用，-1过期，-2已使用）
      CouponType: 0, //0跑腿券，1商品券
    })
      .then(res => {
        setCouponList(e => [...e, ...res.CouponList]);
      })
      .finally(() => setIsLoading(false));
  };
  return (
    <View>
      <SwitchTab
        items={['已使用', '已失效']}
        onChange={handleSwitchTabChange}
      />
      <CouponList
        data={couponList}
        ref={childRef}
        itemType={outDateType === -1 ? 3 : 2}
        ListEmptyText={ListEmptyText}
        onRefresh={handleCouponListRefresh}
        onEndReached={handleEndReached}
      />
    </View>
  );
};

export default HistoricalCoupons;
