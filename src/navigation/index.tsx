import React, {FC} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import type {NativeStackScreenProps} from '@react-navigation/native-stack';

import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';

import MyCoupon from '@src/screens/MyCoupon/index';
import HistoricalCoupons from '@src/screens/HistoricalCoupons/index';
import SelectEntry from '@src/screens/SelectEntry/index';

export type RootStackParamList = {
  MyCoupon: {type: 'user' | 'merchant'; order?: boolean};
  HistoricalCoupons: undefined;
  SelectEntry: undefined;
};

const Stack = createNativeStackNavigator<RootStackParamList>();
// const RootStack = createStackNavigator<RootStackParamList>();

export const RootNavigator: FC = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: '#333333',
        headerTitleStyle: {color: '#1A1A1A'},
        headerShadowVisible: false,
        headerBackTitleVisible: false,
      }}>
      <Stack.Screen
        name="SelectEntry"
        component={SelectEntry}
        options={{
          title: '选择进入',
          contentStyle: {
            backgroundColor: '#F6F6F6',
          },
        }}
      />
      <Stack.Screen
        name="MyCoupon"
        component={MyCoupon}
        options={{
          title: '我的优惠券',
          contentStyle: {
            backgroundColor: '#F6F6F6',
          },
        }}
      />
      <Stack.Screen
        name="HistoricalCoupons"
        component={HistoricalCoupons}
        options={{
          title: '历史优惠券',
          contentStyle: {
            backgroundColor: '#F6F6F6',
          },
        }}
      />
    </Stack.Navigator>
  );
};

export type PropProfileScreenRouteProps<
  Screen extends keyof RootStackParamList = any,
> = NativeStackScreenProps<RootStackParamList, Screen>;
