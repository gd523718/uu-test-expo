import {CouponItme, CouponTypeItem} from '@src/types/Coupon';
import {request} from '@src/utils/http';

// 获取优惠券分裂列表
export const getUserCouponTypes = () =>
  request<{CouponTypeList: CouponTypeItem[]}>(
    'fewebapi/Coupon/GetUserCouponTypes',
  );

export interface GetCouponListParams {
  UserId: number; //用户id
  EnterpriseId: number; //企业券传企业id，个人券传0；历史优惠券传0
  PageIndex: number; //页码
  PageSize: number; //每页数据条数
  PriceToken: string; //计价token，传空字符传 ""
  /** 优惠券类型: 1正常可用，-1过期，-2已使用 */
  OutDate: 1 | -1 | -2;
  /** 优惠券分类 */
  CouponType: number;
}

interface CuoponResponse {
  SoonExpireNum: number;
  ListType: number;
  ListMoney: string;
  CouponList: CouponItme[];
}
// 获取优惠券列表
export const getCouponList = (p: GetCouponListParams) =>
  request<CuoponResponse>('fewebapi/Coupon/GetCouponList', p);
