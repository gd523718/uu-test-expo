import {ResponseBodyType} from '@src/types/index';
import webJm from './webAppJM.js';
const BACE_URL = 'http://m.test.uupt.com/';

const request = async <T = any>(
  api: string,
  params: {[x: string]: any} = {},
) => {
  return new Promise<T>(async (resolve, reject) => {
    try {
      console.log('================================');
      console.log('> request params:');
      console.log(params);
      console.log('================================');

      const {data: parmasData, time} = webJm.JM(JSON.stringify(params) || {});
      fetch(BACE_URL + api + `?t=${time}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Plat: '9',
          ChannelID: '',
          LocationX: '',
          LocationY: '',
          CityName: '',
          CountyName: '',
          T: time || '', // 时间戳
          VerInt: '2090',
          token: 'a6ef64e138ca4d348980f3790df957fb', //用户token
          OToken: '',
          ClientUuid: '',
        },
        body: JSON.stringify({data: parmasData}),
      })
        .then<ResponseBodyType>(response => response.json())
        .then(data => {
          data.Body = data.Body || {};
          // if (data?.State !== 1) {
          console.log('================================');
          console.log('< response data:');
          console.log(data);
          console.log('================================');
          // reject(data);
          // }
          resolve(data.Body);
        })
        .catch(e => reject(e));
    } catch (error) {
      console.error('catch error');
      reject(error);
    }
  });
};
export {request};
