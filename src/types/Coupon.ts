// 优惠券分类
export interface CouponTypeItem {
  CouponType: number;
  Name: string;
}

export interface CouponItme {
  CouponSource: string;
  /** 优惠券金额下文案 */
  CouponName: string;
  /** 优惠券ID */
  CouponID: string;
  CouponGroupCode: string;
  CouponStatus: string;
  /** 满减类型：金额 */
  Amount: string;
  /** 最小使用限制 */
  MinUseLimit: string;
  /** 获取日期 */
  GetDate: string;
  /** 过期时间，距离过期3天内提示 即将过期 */
  ExpireDate: string;
  UseDate: string;
  /** 优惠券名称下的使用限制列表 */
  UseNote: string;
  /** 优惠券数量 */
  Num: string;
  LimitArea: string;
  /** 类型：0满减，1折扣 */
  DiscountType: '0' | '1';
  SendType: string;
  /** 优惠券名称下额度使用限制条件，拼接UseNote列表后展示 */
  DiscountNote: string;
  CouponPic: string;
  /** 优惠券名称 */
  CouponTitle: string;
  SubsendType: string;
  Discount: string;
  ToExpire: number;
}
