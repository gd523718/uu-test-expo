import {View, Text, Image, StyleSheet} from 'react-native';
import React from 'react';

type Props = {
  title: string;
};

const ListEmpty = (props: Props) => {
  return (
    <View style={styles.ListEmpty}>
      <Image source={require('@src/assets/img/MyCoupon/empty/empty.png')} />
      <Text style={styles.title}>{props.title}</Text>
    </View>
  );
};

export default ListEmpty;

const styles = StyleSheet.create({
  ListEmpty: {
    alignItems: 'center',
    paddingTop: 100,
    paddingBottom: 50,
  },
  title: {
    fontSize: 17,
    fontWeight: '400',
    color: '#333',
  },
});
