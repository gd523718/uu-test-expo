import {FlatList, StyleProp, StyleSheet, View, ViewStyle} from 'react-native';
import React, {forwardRef, useImperativeHandle, useRef, useState} from 'react';
import CouponItem from './CouponItem';
import ListEmpty from './ListEmpty';
import {PropProfileScreenRouteProps} from '@src/navigation/index';
import {CouponItme} from '@src/types/Coupon';

type Props = {
  data: CouponItme[];
  selectItem?: CouponItme;
  navigation?: PropProfileScreenRouteProps['navigation'];
  ListFooterComponent?: React.ReactElement;
  ListEmptyText?: string;
  onRefresh?: () => Promise<void>;
  onEndReached?: () => void;
  handleCouponItemClick?: (item: CouponItme) => void;
  itemType: 1 | 2 | 3 | 4;
};

export type CouponListRef = {
  onRefresh: (fn: () => Promise<void>) => Promise<void>;
  setRefreshing?: (b: boolean) => void;
};

const CouponList = forwardRef<CouponListRef, Props>((props, ref) => {
  const [refreshing, setRefreshing] = useState(false);
  const [
    onEndReachedCalledDuringMomentum,
    setOnEndReachedCalledDuringMomentum,
  ] = useState(true);
  let fList: FlatList | null;

  const renderItem = (
    p: {item: CouponItme},
    onPress?: Props['handleCouponItemClick'],
  ) => {
    return (
      <CouponItem
        item={p.item}
        onPress={onPress}
        type={props.itemType}
        selectItem={props.selectItem}
      />
    );
  };

  // 下拉刷新事件
  const handleRefresh = async () => {
    if (refreshing) return;
    setOnEndReachedCalledDuringMomentum(true);
    if (props.onRefresh) {
      setRefreshing(true);
      try {
        await props.onRefresh();
      } finally {
        setRefreshing(false);
      }
    }
  };

  // 触底事件
  const handleEndReached = () => {
    if (!onEndReachedCalledDuringMomentum) {
      setOnEndReachedCalledDuringMomentum(true);
      console.log('handleEndReached 触底');
      props.onEndReached && props.onEndReached();
    }
  };

  // 向外暴露方法
  useImperativeHandle(ref, () => ({
    onRefresh: async fn => {
      setOnEndReachedCalledDuringMomentum(true);
      await fn();
      handleRefresh();
    },
    setRefreshing,
  }));
  return (
    <View style={{height: '100%'}}>
      <FlatList<CouponItme>
        onEndReachedThreshold={0.2}
        keyExtractor={(item, i) => item.CouponID + i}
        onRefresh={handleRefresh}
        onEndReached={({distanceFromEnd}) => {
          if (distanceFromEnd < 0) return;
          handleEndReached();
        }}
        onMomentumScrollBegin={event => {
          setOnEndReachedCalledDuringMomentum(false);
        }}
        extraData={props.data}
        ref={Fref => (fList = Fref)}
        data={props.data}
        refreshing={refreshing}
        ListEmptyComponent={
          <ListEmpty title={props.ListEmptyText || '暂无可用优惠券'} />
        }
        ListFooterComponent={props.ListFooterComponent}
        renderItem={item => renderItem(item, props.handleCouponItemClick)}
        style={styles.couponList}
        contentContainerStyle={styles.couponList.contentContainer}
      />
    </View>
  );
});

CouponList.displayName = 'CouponList';

export default CouponList;

const styles = StyleSheet.create({
  couponList: {
    paddingHorizontal: 17,
    paddingVertical: 10,
    contentContainer: {
      paddingBottom: 200,
    } as StyleProp<ViewStyle>,
  },
});
